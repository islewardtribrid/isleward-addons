// ==UserScript==
// @name         Isleward - Show Login/Logoff
// @namespace    Isleward.Addon
// @version      1.0.2
// @description  When Active it removes login and logoff messages from the chat
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_hide_login-logoff';
                events.on('onGetMessages', this.onGetMessages.bind(this));
            },
            onGetMessages: function() {
                if (typeof window.showLoginLogoff !== 'undefined') {
                    if(window.showLoginLogoff == false ) {
                        return;
                    }
                }
                let x = $('.uiMessages .list.info .info');
                if($(x[x.length-1]).first().text().includes('come online') || $(x[x.length-1]).first().text().includes('gone offline')) {
                    $(x[x.length-1]).remove();
                }
            }
        });
    })
);
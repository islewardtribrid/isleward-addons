# How to use the addons

When using addons without the Additional Settings addon, all addons automatically are active.
When using addons with the Additional Settings addon, all addons stated in the list are toggleable.

Some addons are not toggleable and always active, for example the Free Inventory Slots Counter and the Restyled Status Hud.

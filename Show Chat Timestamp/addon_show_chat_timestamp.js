// ==UserScript==
// @name         Isleward - Show Chat Timestamp
// @namespace    Isleward.Addon
// @version      1.0.2
// @description  Adds a Timestamp infront of each Message
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_chat_timestamp';
                events.on('onGetMessages', this.onGetMessages.bind(this));
            },
            onGetMessages: function(obj) {
                if (typeof window.showTimestamp !== 'undefined') {
                    if(window.showTimestamp == false ) {
                        return;
                    }
                }
                if(obj.messages){
                    if(!obj.messages[0]){
                        obj.messages = [obj.messages];
                    }
                    if(obj.messages[0].message){
                        obj.messages[0].message = "["+(new Date().toLocaleTimeString()).split(":").join("&#58;")+"] "+obj.messages[0].message;
                    }
                }
            }
        });
    })
);
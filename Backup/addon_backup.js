// ==UserScript==
// @name         Isleward - Backup
// @namespace    Isleward.Addon
// @version      1.2.1
// @description  Backs up the inventories of every character into the localstorage after logging in, only once daily
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        let date = new Date();
        addons.register({
            init: function(events) {
                this.id = 'addon_backup';
                events.on('onGetPlayer', this.onGetPlayer.bind(this));
            },
            onGetPlayer: function(obj) {
                if(localStorage[obj.name] === undefined) {
                    localStorage[obj.name] = `${date.getDate()} ${date.getMonth()} ${date.getFullYear()} ||| ${JSON.stringify(obj?.inventory?.items)}`;
                    console.log(`Created ${obj.name} Backup`);
                }
                if(localStorage[obj.name].startsWith(date.getDate())) {
                    return;
                } else {
                    localStorage[obj.name] = `${date.getDate()} ${date.getMonth()} ${date.getFullYear()} ||| ${JSON.stringify(obj?.inventory?.items)}`;
                    console.log(`Updated ${obj.name}`);
                }
            }
        })    
    })
);
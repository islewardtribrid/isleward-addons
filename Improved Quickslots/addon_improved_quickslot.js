// ==UserScript==
// @name         Isleward - Improved Quickslot Addon
// @namespace    Isleward.Addon
// @version      1.1.3
// @description  Combines Diswards Horse Switcher, a cooldown display and a fish stack display
// @author       Horse Switcher initially created by Disasm, broken down into the cd and switcher addon by Tribrid, cd addon expanded by Tribrid and Fish Stack by Tribrid
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function (events) {
                this.id = 'addon_improved_quickslots';
                events.on('onGetItems', this.onGetItems.bind(this));
            },
            onGetItems: function (obj) {

                for (let item_id in obj) {
                    item = obj[item_id]
                    if ('quickSlot' in item) {
                        if (item.cd) {
                            $(".quickItem .info").text("R " + item.cd);
                        } else if (!item.cd) {
                            if (item.type == 'consumable') {
                                if (item.name.includes('on a Stick')) {
                                    this.totalfish = 0
                                    for (let specFish in obj) {
                                        if (obj[specFish].name == item.name) {
                                            this.totalfish = this.totalfish + obj[specFish].quantity;
                                        }
                                        $(".quickItem .info").text("R " + this.totalfish);
                                    }
                                } else {
                                    return;
                                }
                            } else {
                                return;
                            }
                        }

                    }
                }

                for (let index = 0; index < window?.player?.effects?.effects?.length; index++) {
                    if (window?.player?.effects?.effects[index]?.type == 'mounted') {
                        this.alreadyIsMounted = true;
                    }
                }
                if (this.alreadyIsMounted == true) {
                    return;
                }

                for (let item_id in obj) {
                    item = obj[item_id]
                    if ('quickSlot' in item) {
                        this.isCurrentQuickSlotMount = item.type === "mount";
                        this.isCurrentQuickSlotCd = item.cd;
                        break
                    }
                }
                if (this.isCurrentQuickSlotCdOk === 0)
                    return

                if (!this.isCurrentQuickSlotMount)
                    return

                this.bestHorseId = -1;
                this.bestHorseCd = 999999;
                this.currentHorseId = -1;
                for (let item_id in obj) {
                    item = obj[item_id]
                    if (item.type === "mount") {
                        if (item.cd < this.bestHorseCd) {
                            this.bestHorseId = item.id;
                            this.bestHorseCd = item.cd;
                        }
                        if ("quickSlot" in item) {
                            this.currentHorseId = item.id
                        }
                    }
                }
                if (this.bestHorseId === -1)
                    return
                if (this.bestHorseId === this.currentHorseId)
                    return

                if (!this.client) this.client = require("js/system/client");
                this.client.request({
                    cpn: 'player',
                    method: 'performAction',
                    data: { cpn: 'equipment', method: 'setQuickSlot', data: { "itemId": this.bestHorseId, "slot": 0 } }
                });
            }
        });
    })
);
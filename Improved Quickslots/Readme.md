# Improved Quickslots

Combines my standalone Horse Switcher with a cooldown feature and a Fish Stack feature, showing the amount of the currently quickslotted fish on stick.

![''](image1.png)
![''](image2.png)
![''](image3.png)
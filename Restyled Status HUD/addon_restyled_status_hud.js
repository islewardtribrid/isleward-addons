// ==UserScript==
// @name         Isleward - Restyled Status HUD
// @namespace    Isleward.Addon
// @version      1.1.0
// @description  Changes the size of the Status Hud, position of quickslot and target and adds more stats to the Status HUD
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        let newStatusSize = 500; //change this value  to change the size of the status HUD, - Unit: px
                         //default was 280 at me
        addons.register({
            init: function(events) {
                this.id = 'addon_restyled_status_hud';
                events.on('onGetStats', this.onGetStats.bind(this));
            },
            onGetStats: function(obj) {

                if(!$('.ui-container .uiHud .hudBox .boxes .statBox .addon-Text')[0]) {

                    this.hudBox = $('.ui-container .uiHud .hudBox')
                    this.hudBox.css({
                        'width': `calc(${newStatusSize}px + (8px * 2))`
                    });
                    this.quickBar = $('.ui-container .uiHud .quickBar')
                    this.quickBar.css({
                        'left': `calc(${newStatusSize + 10}px + (8px * 2))`
                    });
                    this.uiTarget = $('.ui-container .uiTarget')
                    this.uiTarget.css({
                        'left': `${newStatusSize+(2*8)+10+10+80+10}px`
                    });
                    this.statBoxText = $('.ui-container .uiHud .hudBox .boxes .statBox .text').css({
                        'display': 'none'
                    });
                    this.statBox = $('.ui-container .uiHud .hudBox .boxes .statBox')

                    this.uiNewText = $('<div id = "addon-Text" class="addon-Text"></div>').appendTo(this.statBox);
                    this.uiNewText.css({
                        'position': 'absolute',
                        'left': '0',
                        'top': '0',
                        'width': '100%',
                        'height': '100%',
                        'text-align': 'center',
                        'color': '#fcfcfc',
                        'padding': '2px 0',
                        'line-height': '16px',
                        'text-shadow': '2px 0 #2d2136, -2px 0 #2d2136, 0 -2px #2d2136, 0 2px #2d2136, -2px -2px #2d2136, -2px 2px #2d2136, 2px -2px #2d2136, 2px 2px #2d2136'
                    });
                }
                $('.ui-container .uiHud .hudBox .boxes .statBox .addon-Text')[0].innerText = `${Math.round(obj.hp)}/${Math.round(obj.hpMax)} || ${(obj.regenHp*0.2).toFixed(2)} Regen/Tick`;
                $('.ui-container .uiHud .hudBox .boxes .statBox .addon-Text')[1].innerText = `${Math.round(obj.mana)}/${Math.round(obj.manaMax)} || ${(obj.regenMana/50).toFixed(2)} Regen/Tick`;
                if(obj.level < 23) {
                    $('.ui-container .uiHud .hudBox .boxes .statBox .addon-Text')[2].innerText = `Level: ${obj.level} || ${obj.xp}/${obj.xpMax} until Level ${obj.level+1}`;
                } else {
                $('.ui-container .uiHud .hudBox .boxes .statBox .addon-Text')[2].innerText = `Level: ${obj.level} || ${obj.xpTotal} Total EXP`;
                }
            }
        })    
    })
);
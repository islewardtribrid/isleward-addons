# Restyled Status HUD

Changes the size of the Status Hud, position of quickslot and target and adds more stats to the Status HUD.

To change the width to your liking, edit the value 'newStatusSize' in the code, Unit is px.

![''](image.png)

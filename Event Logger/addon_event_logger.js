// ==UserScript==
// @name         Isleward - Event Object logger
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Logs the obj a event returns when switching its representing value from false to true in the console
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 10000);
    }
}
defer(
    (function () {
        for (let index = 0; index < Object.entries(window?.addons?.events?.events).length; index++) {
            window[`${Object.entries(window?.addons?.events?.events)[index][0]}Log`] = false;
            addons.register({
                init: function(events) {
                    events.on(`${Object.entries(window?.addons?.events?.events)[index][0]}`, function(obj) {
                        if(window[`${Object.entries(window?.addons?.events?.events)[index][0]}Log`] == true) {
                            console.log(obj);
                        }
                    })
                }  
            }) 
        }
    })
);

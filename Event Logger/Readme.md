# Event Logger

With this addon you can quickly toggle the logging of events by switching the value of the wanted event in the console to true.
For example, if you want to log everything you get from the 'onGetObject' event, type this in the console: 'onGetObjectLog = true'. From now on all 'onGetObject' events will be logged, to turn it off again simply switch it back to 'false'



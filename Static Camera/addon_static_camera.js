// ==UserScript==
// @name         Isleward - Static Camera Switcher
// @namespace    Isleward.Addon
// @version      1.0.5
// @description  Lets you toggle between a static and dynamic Camera by pressing the Icon in the menubar
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

window.staticCamera = false;

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_static_camera';
                events.on('onGetStats', this.onGetStats.bind(this));
            },
            onGetStats: function() {

                if (typeof(document.getElementById('camera')) === 'undefined' || document.getElementById('camera') === null) {

                    $('.ui-container .uiMenu .break').remove();

                    this.button = $(`<div id = "camera" class="camera"></div>`).appendTo($('.ui-container .uiMenu'));
                    this.button.css({
                        'width': '64px',
                        'height': '64px',
                        'float': 'left',
                        'cursor': 'pointer',
                        'position': 'relative;'
                    });
                    this.buttonIcon = $(`<div id = "cameraIcon" class="cameraIcon"></div>`).appendTo($('.ui-container .uiMenu .camera'));
                    this.buttonIcon.css({
                        'pointer-events': 'none',
                        'position': 'relative',
                        'left': '0',
                        'top': '0',
                        'width': '64px',
                        'height': '64px',
                        'background': 'url(../../../images/uiIcons.png) -256px -64px'
                    });
                    
                    document.getElementById(`camera`).addEventListener('click', function() {
                        if(window.staticCamera == true) {
                            window.staticCamera = false;
                        } else
                        if(window.staticCamera == false) {
                            window.staticCamera = true;
                        }    
                    });
                }
            }
        })    
    })
);
// ==UserScript==
// @name         Isleward - Wide Menu
// @namespace    Isleward.Addon
// @version      1.0.3
// @description  Changes the size of the menu
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_wide_menu';
                events.on('onGetStats', this.onGetStats.bind(this));
            },
            onGetStats: function() {
                $('.ui-container .uiMenu .break').remove();

                $('.ui-container .uiMenu').css({
                    'width': '532px',
                    'height': '84px'
                })
            }
        })    
    })
);
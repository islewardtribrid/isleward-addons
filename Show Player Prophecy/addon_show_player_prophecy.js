// ==UserScript==
// @name         Isleward - Show Player Prophecies
// @namespace    Isleward.Addon
// @version      1.1.4
// @description  Displays the Prophecies of players, including your own prophecy
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_player_prophecy';
                events.on('onGetObject', this.onGetObject.bind(this));
            },
            onGetObject: function(obj) {
                if (typeof window.showProphecy !== 'undefined') {
                    if(window.showProphecy == false ) {
                        return;
                    }
                }
                obj?.components?.forEach(element => {
                    if(element.type =='prophecies') {
                        if(element.list.length == 0) {
                            return;
                        }
                        if(obj.components.self) {
                            return;
                        }
                        if(obj.self) {
                            return;
                        }
                        
                        obj.name = obj.name + '\n'
                        element.list.forEach(prophecy => {

                            if(prophecy == 'butcher') {
                                console.log(obj)
                                obj.name = obj.name +'⚔️';
                            }
                            if(prophecy == 'austere') {
                                obj.name = obj.name + '🧹';
                            }
                            if(prophecy == 'hardcore') {
                                obj.name = obj.name + '💀';
                            }
                            if(prophecy == 'crushable') {
                                obj.name = obj.name + '🦴';
                            }
                        })
                    }
                });
            }
        });
    })
);


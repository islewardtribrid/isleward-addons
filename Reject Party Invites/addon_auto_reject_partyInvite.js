// ==UserScript==
// @name         Isleward - Auto reject Party Invite
// @namespace    Isleward.Addon
// @version      1.0.2
// @description  Automatically rejects Party Invites
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_auto_reject_party_invite';
                events.on('onGetInvite', this.onGetInvite.bind(this));
            },
            onGetInvite: function() {
                if (typeof window.rejectPartyInvite !== 'undefined') {
                    if(window.rejectPartyInvite == false ) {
                        return;
                    }
                }
                setTimeout(()=>{
                    $('.ui-container .uiParty .invite .buttons .btn').eq(0).click()
                }, 50);
            }
        });
    })
);
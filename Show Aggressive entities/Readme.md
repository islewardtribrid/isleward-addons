# Show Aggressive Entities

Displays whether or not a enemy is aggressive.

![''](image.png)

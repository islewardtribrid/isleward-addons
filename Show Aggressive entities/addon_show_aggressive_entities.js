// ==UserScript==
// @name         Isleward - Show Aggressive Enemies
// @namespace    Isleward.Addon
// @version      1.0.2
// @description  Displays whether or not a enemy is aggressive
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_aggressive_entities';
                events.on('onGetObject', this.onGetObject.bind(this));
            },
            onGetObject: function(obj) {
                if (typeof window.showAggressive !== 'undefined') {
                    if(window.showAggressive == false ) {
                        return;
                    }
                }
                obj?.components?.forEach(element => {
                    if(element.type =='aggro') {
                        if(element.faction == 'hostile') {
                            obj.name = '❗️ ' + obj.name;
                        } 
                    }
                });
            }
        });
    })
);
// ==UserScript==
// @name         Isleward - Player Name Color
// @namespace    Isleward.Addon
// @version      1.0.1
// @description  Changes the player name color clientside
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_player_name_color';
                events.on('onGetPlayer', this.onGetPlayer.bind(this));
            },
            onGetPlayer: function(obj) {
                obj.nameSprite.style.fill = '#a24eff';
            }
        });
    })
);

# Custom Name Color

Changes the Players namecolor clientside.

Change the color to your liking by editing the text after obj.nameSprite.style.fill.

https://htmlcolorcodes.com/color-picker/

Make sure to copy the HEX value.

!['Test'](image.png)

// ==UserScript==
// @name         Isleward - Quick Stash
// @namespace    Isleward.Addon
// @version      1.0.2
// @description  Allows players to quickly stash items by hovering over the item and pressing b
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
(function () {
    window.quickStashData = {};
    window.quickStashData.itemPos = -1;
    addons.register({
        init: function(events) {
            this.id = 'addon_quick_stash';
            events.on('onShowItemTooltip', this.onShowItemTooltip.bind(this));
            events.on('onHideItemTooltip', this.onHideItemTooltip.bind(this));
            events.on('onKeyDown', this.onKeyDown.bind(this));
        },
        onShowItemTooltip: function(obj) {
            window.quickStashData.itemPos = obj.pos;
        },
        onHideItemTooltip: function() {
            window.quickStashData.itemPos = -1;
        },
        onKeyDown: function(key) {
            if (!key) {
                return;
            } else if (key == "b") {
                if(jQuery(".ui-container .uiInventory").css("display") == "block" && window.quickStashData.itemPos != -1 && typeof jQuery(".uiMessages .active .typing")[0] === "undefined"){
                    jQuery(".ui-container .uiInventory .grid .item").eq(window.quickStashData.itemPos).find(".icon").contextmenu();
                    for(var i=0;i< $(".uiContext .list .option").length;++i){
                        if(jQuery(".uiContext .list .option").eq(i).text() == "stash"){
                            jQuery(".uiContext .list .option").eq(i).click();
                            break;
                        }
                    }
                }
            }
        },
    });
}));

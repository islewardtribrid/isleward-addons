// ==UserScript==
// @name         Isleward - Restyled Character Menu
// @namespace    Isleward.Addon
// @version      1.0.2
// @description  Increases the amount of characters shown in the menu
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_restyled_character_menu';
                events.on('onShowCharacterSelect', this.onShowCharacterSelect.bind(this));
            },
            onShowCharacterSelect: function() {

                setTimeout(() => {
                    $('.ui-container .uiCharacters .logo').detach();
                    $('.ui-container .uiCharacters').css({
                        'left': '0px',
                        'top': '0px',
                        'height': '100%',
                        'width': '100%'
                    });
                    $('.ui-container .uiCharacters .bottom').css({
                        'height': '100%'
                    });
                }, 500);
            }
        });
    })
);
// ==UserScript==
// @name         Isleward - Multiple Accounts quickpaste
// @namespace    Isleward.Addon
// @version      1.0.3
// @description  Saves the credentials of 3 seperate accounts and pastes them upon click
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
(function () {
    addons.register({
        init: function(events) {
            this.id = 'addon_account_slots';
            events.on('onResourcesLoaded', this.onResourcesLoaded.bind(this));
            events.on('onGetPlayer', this.onGetPlayer.bind(this));
        },
        onResourcesLoaded: function() {

            let accountNames = ['Account 1 Name', 'Account 2 Name', 'Account 3 Name'];
            let accountPasswords = ['Account 1 Password', 'Account 1 Password', 'Account 1 Password'];
            
            for (let index = 0; index < accountNames.length; index++) {
                                
                this.charButton = $(`<div id = "charButton${index}" class="charButton${index}">${accountNames[index]}</div>`).appendTo($('.ui-container .right'));
                this.charButton.css({
                    'cursor': 'pointer',
                    'display': 'flex',
                    'justify-content': 'center',
                    'background-color': '#3a71ba',
                    'float': 'left',
                    'margin-right': '16px',
                    'color': '#fcfcfc',
                    'align-items': 'center',
                    'width': '200px',
                    'height': '35px'
                });
  
                document.getElementById(`charButton${index}`).addEventListener('click', function() {
                    $(".txtUsername").val(accountNames[index]);
                    $(".txtPassword").val(accountPasswords[index]);
                });
            }
        },
        onGetPlayer: function() {
            for (let index = 0; index < $('.ui-container .right')[0].childElementCount; index++) {
                if(document.getElementById(`charButton${index}`) == null) {
                    return;
                }
                document.getElementById(`charButton${index}`).remove();
            }
        }
    });
}));

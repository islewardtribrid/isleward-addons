# Account Slots

This Addon adds Buttons to the login screen, which, after been pressed, paste the credentials of the selected account into the login fields.

Change the values of 'accountNames' and 'accountPasswords'.

![''](image.png)

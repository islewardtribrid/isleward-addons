// ==UserScript==
// @name         Isleward - Show Faction Reputation
// @namespace    Isleward.Addon
// @version      1.0.1
// @description  Displays the amount of Reputation you have for each faction in the faction menu
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function() {
                this.id = 'addon_faction_reputation';
            },
        });

        let repeat2 = function() {
            if (typeof window.showReputation !== 'undefined') {
                if(window.showReputation == false ) {
                    return;
                }
            }
            
            if (!$('.ui-container .uiReputation .list .faction').length) {
                return;
            }
            if ($('.ui-container .uiReputation').css('display') == 'none') {
                return;
            }

            let faction;
            let factionTier;
            let factionRep;
            let repNumber;

            for (let i = 0; i < $('.ui-container .uiReputation .list .faction').length; i++) {
                if ($('.ui-container .uiReputation .list .faction')[i].className == 'faction selected') {
                    faction = $('.ui-container .uiReputation .list .faction')[i].innerText;
                }
            }

            window.player.reputation.list.forEach(element => {
                if (element.name == faction) {
                    factionRep = element.rep;
                    factionTier = element.tier;
                }
            });

            repNumber = $('.ui-container .uiReputation .info .bar-outer .tier').text();

            if (repNumber.includes(factionRep)) {
                return;
            } else {
                let tiers = ['-25000', '-10000', '-1000', '0', '1000', '10000', '25000', '50000', '50000']
                repNumber = repNumber + ' ' + factionRep + '/' + tiers[factionTier+1];
            }

            $('.ui-container .uiReputation .info .bar-outer .tier').text(repNumber);
        
        };
        setInterval(repeat2, 1000);
    })
);

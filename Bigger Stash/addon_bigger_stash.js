// ==UserScript==
// @name         Isleward - Bigger Stash
// @namespace    Isleward.Addon
// @version      1.0.2
// @description  Changes the size of Private Stash aswell as the CrewStash
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_bigger_stash';
                events.on('onGetStats', this.onGetStats.bind(this));
            },
            onGetStats: function() {
                $('.ui-container .uiStash').css({
                    'top': '0px',
                    'height': '100%'
                });
                $('.ui-container .uiStash .grid').css({
                    'height': '92%'
                });
            }
        })  
    })
);
// ==UserScript==
// @name         Isleward - Show Decoy Duration
// @namespace    Isleward.Addon
// @version      1.0.3
// @description  Displays the remaining ticks of a decoy as its name instead the word 'decoy'
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        let decoyDuration;

        function startCountdownDecoy(idX, durationX) {
            var repeat = function(){
                if(durationX > 0) {
                    durationX--;
                    window.addons.events.emit("onGetObject", {id: idX, name: `${durationX}`});
                }
            };
            setInterval(repeat, 350);       
        }
        addons.register({
            init: function(events) {
                this.id = 'addon_decoy_duration';
                events.on('onGetObject', this.onGetObject.bind(this));
                events.on('onGetItems', this.onGetItems.bind(this));
            },
            onGetItems: function (inv) {
                if (!inv) return;
                for (let item in inv) {
                    if (inv[item].name === "Nyxaliss' Nock") {
                        decoyDuration = inv[item].effects[0].rolls.castSpell.duration;
                    }
                }
            },
            onGetObject: function(obj) {
                if (typeof window.showDecoyDuration !== 'undefined') {
                    if(window.showDecoyDuration == false ) {
                        return;
                    }
                }
                if (!obj) {
                    return;
                }
                if (obj.name === "Decoy") {
                    startCountdownDecoy(obj.id, decoyDuration);                
                }      
            }
        });
    })
);
# Decoy Duration

Displays the remaining ticks of a decoy as its name instead the word 'decoy'.

![''](image.png)

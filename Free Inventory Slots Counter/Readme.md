# Free Inventory Slots Counter

Shows the amount of free item slots next to the inventory icon at the bottom left.

![''](image.png)

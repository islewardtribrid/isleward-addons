// ==UserScript==
// @name         Isleward - Show Rezone Announcements
// @namespace    Isleward.Addon
// @version      1.4.0
// @description  Displays a Announcement whenever you entered a area
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        let zone;
        let showAmount = 0;
        addons.register({
            init: function(events) {
                this.id = 'addon_rezone_announcement';
                events.on('onGetMap', this.onGetMap.bind(this));
                events.on('onGetPlayer', this.onGetPlayer.bind(this));
                events.on('onShowCharacterSelect', this.onShowCharacterSelect.bind(this));
            },
            onGetPlayer: function() {
                if (typeof(document.getElementById('rezoneAnnouncement')) === 'undefined' || document.getElementById('rezoneAnnouncement') === null) {
                    this.rezoneAnnouncement = $('<div id = "rezoneAnnouncement" class="rezoneAnnouncement"></div>').appendTo($('.ui-container'));
                    this.rezoneAnnouncement.css({
                        'width': '75%',
                        'margin': 'auto',
                        'text-align': 'center',
                        'color': '#c0c3cf',
                        'top': '30%',
                        'position': 'absolute',
                        'font-size': '3em',
                        'filter': 'drop-shadow(0 -10px 0 #312136) drop-shadow(0 10px 0 #312136) drop-shadow(10px 0 0 #312136) drop-shadow(-10px 0 0 #312136)',
                        'display': 'none'
                    });
                }
            },
            onShowCharacterSelect: function() {
                showAmount = 0;
            },
            onGetMap: function() {
            setTimeout(() => { 
                if (typeof window.showRezoneAnnouncement !== 'undefined') {
                    if(window.showRezoneAnnouncement == false ) {
                        return;
                    }
                }
                showAmount = showAmount + 1;
                if(showAmount < 2) {
                    return;
                }

                if(window?.player?.zoneName === 'cave') {
                    zone = 'Crystal Cave'
                } else
                if(window?.player?.zoneName === 'fjolarok') {
                    zone = 'Fjolarok';
                } else
                if(window?.player?.zoneName === 'estuary') {
                    zone = 'Estuary';
                } else
                if(window?.player?.zoneName === 'fjolgard') {
                    zone = 'Fjolgard';
                } else
                if(window?.player?.zoneName === 'fjolgardHousing') {
                    zone = 'Fjolgard Housing';
                } else
                if(window?.player?.zoneName === 'sewer') {
                    zone = 'Sewer';
                } else
                if(window?.player?.zoneName === 'blisterwind') {
                    zone = 'Blisterwind';
                } else
                if(window?.player?.zoneName === 'abyssalTrial') {
                    zone = 'Trials Of The Abyss';
                } else 
                if(window?.player?.zoneName === 'templeOfGaekatla') {
                    zone = 'Temple Of Gaekatla';
                } else
                if(windo?.player?.zoneName === 'abyssalTrial') {
                    zone = 'Abyssal Trial';
                }

                if (document.getElementById(`rezoneAnnouncement`).style['display'] = 'none') {
                    document.getElementById(`rezoneAnnouncement`).style['display'] = 'block';
                    document.getElementById('rezoneAnnouncement').innerText = `You entered ${zone}`;

                    }  
                if (document.getElementById(`rezoneAnnouncement`).style['display'] = 'block') {
                    setTimeout(() => {
                        document.getElementById(`rezoneAnnouncement`).style['display'] = 'none';
                
                    }, 2000);
                }
            },1000);
            }    
        });
    })
);
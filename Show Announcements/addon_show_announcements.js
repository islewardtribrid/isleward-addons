// ==UserScript==
// @name         Isleward - Show Annoumcements
// @namespace    Isleward.Addon
// @version      2.1.2
// @description  Shows announcements regarding Plague of rats, ingar restocking and finn appearing
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_announcements';
                events.on('onGetPlayer', this.onGetPlayer.bind(this));
            }, 
            onGetPlayer: function() {
                
                if (typeof(document.getElementById('finnAnnouncement')) === 'undefined' || document.getElementById('finnAnnouncement') === null) {
                    this.finnAnnouncement = $('<div id = "finnAnnouncement" class="finnAnnouncement">Finn Appears in 3 Minutes!</div>').appendTo($('.ui-container'));
                    this.finnAnnouncement.css({
                        'width': '75%',
                        'margin': 'auto',
                        'text-align': 'center',
                        'color': '#4ac441',
                        'top': '30%',
                        'position': 'absolute',
                        'font-size': '3em',
                        'filter': 'drop-shadow(0 -10px 0 #312136) drop-shadow(0 10px 0 #312136) drop-shadow(10px 0 0 #312136) drop-shadow(-10px 0 0 #312136)',
                        'display': 'none'
                    });
                }
                if (typeof(document.getElementById('plagueAnnouncement')) === 'undefined' || document.getElementById('plagueAnnouncement') === null) {
                    this.plagueAnnouncement = $('<div id = "plagueAnnouncement" class="plagueAnnouncement">Plague of Rats starts in 3 Minutes!</div>').appendTo($('.ui-container'));
                    this.plagueAnnouncement.css({
                        'width': '75%',
                        'margin': 'auto',
                        'text-align': 'center',
                        'color': '#4ac441',
                        'top': '35%',
                        'position': 'absolute',
                        'font-size': '3em',
                        'filter': 'drop-shadow(0 -10px 0 #312136) drop-shadow(0 10px 0 #312136) drop-shadow(10px 0 0 #312136) drop-shadow(-10px 0 0 #312136)',
                        'display': 'none'
                    });
                }
                if (typeof(document.getElementById('ingarAnnouncement')) === 'undefined' || document.getElementById('ingarAnnouncement') === null) {
                    this.ingarAnnouncement = $('<div id = "ingarAnnouncement" class="ingarAnnouncement">Ingar Restocks in 3 Minutes!</div>').appendTo($('.ui-container'));
                    this.ingarAnnouncement.css({
                        'width': '75%',
                        'margin': 'auto',
                        'text-align': 'center',
                        'color': '#4ac441',
                        'top': '40%',
                        'position': 'absolute',
                        'font-size': '3em',
                        'filter': 'drop-shadow(0 -10px 0 #312136) drop-shadow(0 10px 0 #312136) drop-shadow(10px 0 0 #312136) drop-shadow(-10px 0 0 #312136)',
                        'display': 'none'
                    });           
                }
            }
        });
        let repeat = function() {
            if (typeof window.showAnnouncements !== 'undefined') {
                if(window.showAnnouncements == false ) {
                    return;
                }
            }
            let [timeHours, timeMinutes, timeSeconds] = [new Date().getUTCHours(), new Date().getUTCMinutes(), new Date().getUTCSeconds()];
            let finnHours = [23, 3, 7, 11, 15, 19];
            let plagueHours = [23, 2, 5, 8, 11, 14, 17, 20];
            let ingarHours = [23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];
            
            if(finnHours.includes(timeHours) && timeMinutes == 57 && timeSeconds <= 10) {
                if (document.getElementById(`finnAnnouncement`).style['display'] = 'none') {
                    document.getElementById(`finnAnnouncement`).style['display'] = 'block';
                }  
            } else 
            if (document.getElementById(`finnAnnouncement`).style['display'] = 'block') {
                document.getElementById(`finnAnnouncement`).style['display'] = 'None';
            }
            
            if(plagueHours.includes(timeHours) && timeMinutes == 57 && timeSeconds <= 10) {
                if (document.getElementById(`plagueAnnouncement`).style['display'] = 'none') {
                    document.getElementById(`plagueAnnouncement`).style['display'] = 'block';
                }  
            } else 
            if (document.getElementById(`plagueAnnouncement`).style['display'] = 'block') {
                document.getElementById(`plagueAnnouncement`).style['display'] = 'none';
            }
            
            if(ingarHours.includes(timeHours) && timeMinutes == 57 && timeSeconds <= 10) {
                if (document.getElementById(`ingarAnnouncement`).style['display'] = 'none') {
                    document.getElementById(`ingarAnnouncement`).style['display'] = 'block';
                }  
            } else
            if(ingarHours.includes(timeHours) && timeMinutes == 27 && timeSeconds <= 10) {
                if (document.getElementById(`ingarAnnouncement`).style['display'] = 'none') {
                    document.getElementById(`ingarAnnouncement`).style['display'] = 'block';
                }  
            } else 
            if (document.getElementById(`ingarAnnouncement`).style['display'] = 'block') {
                document.getElementById(`ingarAnnouncement`).style['display'] = 'none';
            }
        };
        setInterval(repeat, 1000);
    })
);
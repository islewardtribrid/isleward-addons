// ==UserScript==
// @name         Isleward - Show Faction Reputation Overall
// @namespace    Isleward.Addon
// @version      1.1.1
// @description  Displays the amount of Reputation you have for each faction in the faction menu in a seperate section next to the existing list
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        let replist = [];
        let tierlist = [];

        addons.register({
            init: function(events) {
                this.id = 'addon_faction_reputation_overall';
                events.on('onShowReputation', this.onShowReputation.bind(this));
            },
            onShowReputation: function() {
                if (typeof window.showReputationOverall !== 'undefined') {
                    if(window.showReputationOverall == false ) {
                        return;
                    }
                }

                replist = [];
                tierlist = [];

                window.player.reputation.list.forEach(element=> {
                    if(element.id == 'hostile') {
                        return;
                    }
                    if(element.id == 'players') {
                        return;
                    }
                    replist.push(element.rep);
                    tierlist.push(element.tier);
                })

                if (!$('.ui-container .uiReputation .list .faction').length) {
                    return;
                }
                $('.ui-container .uiReputation .info').remove();

                if (typeof(document.getElementById(`addon-factionList`)) === 'undefined' || document.getElementById(`addon-factionList`) === null) {
                    this.factionList = $(`<div id = "addon-factionList" class="addon-factionList"></div>`).appendTo($('.ui-container> .uiReputation'));
                        this.factionList.css({
                            'width' : '60%',
                            'padding' : '15px 15px 15px 15px',
                            'background-color' : 'rgba(55, 48, 65, 0.9)',
                            'float': 'left',
                            'height': 'calc(100% - 36px)'
                        });
                    }

                for (let index = 0; index < $('.ui-container .uiReputation .list .faction').length; index++) {
                    let tiers = ['-25000', '-10000', '-1000', '0', '1000', '10000', '25000', '50000', '50000'];
                    let reputationText = replist[index] + '/' + tiers[tierlist[index]+1];

                    if (typeof(document.getElementById(`addon-factionBar${index}`)) === 'undefined' || document.getElementById(`addon-factionBar${index}`) === null) {

                        this.factionBar = $(`<div id = "addon-factionBar${index}" class="addon-factionBar${index}">${reputationText}</div>`).appendTo($('.ui-container> .uiReputation .addon-factionList'));
                        this.factionBar.css({
                            'width': '100%',
                            'padding': '5px 10px',
                            'background-color': '#7a3ad3',
                            'color': '#fcfcfc',
                            'margin-bottom': '15px'
                        });
                    } else {
                        $(`.ui-container> .uiReputation .addon-factionList .addon-factionBar${index}`)[0].innerText = reputationText;
                    }
                }
            }
        });
    })
);

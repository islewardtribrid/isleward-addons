# Show Faction Reputation Overall

Displays the amount of Reputation you have for each faction in the faction menu in a seperate section next to the existing list.

![''](image.png)

// ==UserScript==
// @name         Isleward - Auto reject Friend Request
// @namespace    Isleward.Addon
// @version      1.0.2
// @description  Automatically rejects Friend Requests
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_auto_reject_friend_request';
                events.on('getConfirmRequest', this.getConfirmRequest.bind(this));
            },
            getConfirmRequest: function(obj) {
                if (typeof window.rejectFriendRequest !== 'undefined') {
                    if(window.rejectFriendRequest == false ) {
                        return;
                    }
                }
                 if(obj.msg.heading == 'Confirm friend request') {
                    setTimeout(()=>{
                        $('.ui-container .uiConfirmAction .requestBox .toolbar .btn').eq(0).click()
                    }, 50);
               }
            }
        });
    })
);


// ==UserScript==
// @name         Isleward - Show Coordinates
// @namespace    Isleward.Addon
// @version      1.0.2
// @description  Displays the Coordinates of the Player below the clock
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_coordinates';
                events.on('onGetObject', this.onGetObject.bind(this));
                events.on('onGetPlayer', this.onGetPlayer.bind(this));
            },
            onGetPlayer: function() {
                if (typeof(document.getElementById('uiCoordinates')) === 'undefined' || document.getElementById('uiCoordinates') === null) {
                
                    this.uiCoordinates = $('<div id = "uiCoordinates" class="uiCoordinates">sdf</div>').appendTo($('.ui-container .uiMessages .main'));
                    this.uiCoordinates.css({
                        'display': 'flex',
                        'align-items': 'center',
                        'padding-left': '10px',
                        'background-color': 'transparent',
                        'height': '35px',
                        'color': '#fcfcfc',
                        'text-align': 'left',
                        'filter': 'drop-shadow(0 -2px 0 #312136) drop-shadow(0 2px 0 #312136) drop-shadow(2px 0 0 #312136) drop-shadow(-2px 0 0 #312136)',
                        '-moz-filter': 'drop-shadow(0 -2px 0 #312136) drop-shadow(0 2px 0 #312136) drop-shadow(2px 0 0 #312136) drop-shadow(-2px 0 0 #312136)'
                    });                
                }
            },
            onGetObject: function(obj) {
                if (typeof(document.getElementById('uiCoordinates')) === 'undefined' || document.getElementById('uiCoordinates') === null){
                    return;
                }

                if (typeof window.showCoordinates !== 'undefined') {
                    if(window.showCoordinates == false ) {
                        document.getElementById('uiCoordinates').style.display = 'none';
                    return;
                    }
                    else{
                        document.getElementById('uiCoordinates').style.display = 'flex';
                    }
                }
                if (obj?.id == window?.player?.id) {
                    document.getElementById('uiCoordinates').style.display = 'flex';
                    $('.ui-container .uiMessages .main .uiCoordinates').html("X = " + window.player.x + " Y = " + window.player.y); 
                }
            },
        });
    })
);
// ==UserScript==
// @name         Isleward - Object Counter
// @namespace    Isleward.Addon
// @version      1.0.2
// @description  Displays the ammount of nearby Players, Fishing Spots, Herbs and Players
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {      
        var mobIdSet = new Set();
        var carpIdSet = new Set();
        var playerIdSet = new Set();
        var herbIdSet = new Set();
        addons.register({
            init: function(events) {
                this.id = 'addon_object_counter';
                events.on('onGetObject', this.onGetObject.bind(this));
                events.on('onGetPlayer', this.onGetPlayer.bind(this));
                events.on('onGetMap', this.onGetMap.bind(this));
            },
            onGetPlayer: function() {
                if (typeof(document.getElementById('uiObjectCounter')) === 'undefined' || document.getElementById('uiObjectCounter') === null) {

                    this.uiObjectCounter = $('<div id = "uiObjectCounter" class="uiObjectCounter"></div>').appendTo($('.ui-container'));
                    this.uiObjectCounter.css({
                        'right': '430px',
                        'width': 'calc(602px + (8px * 2))',
                        'display': 'block',
                        'top': '16px',
                        'padding': '8px',
                        'background-color': 'rgba(55,48,65,.9)',
                        'box-shadow': '0 -2px 0 #2d2136, 0 2px 0 #2d2136, 2px 0 0 #2d2136, -2px 0 0 #2d2136',
                        'position': 'absolute'
                    });
                    this.uiObjectCounterText = $('<div id = "uiObjectCounterText" class="uiObjectCounterText"></div>').appendTo(this.uiObjectCounter);
                    this.uiObjectCounterText.css({
                        'width': 'calc(100%)',
                        'float': 'left',
                        'height': '100%',
                        'color': '#fcfcfc',
                        'text-align': 'center',
                        'filter': 'drop-shadow(0 -2px 0 #312136) drop-shadow(0 2px 0 #312136) drop-shadow(2px 0 0 #312136) drop-shadow(-2px 0 0 #312136)',
                        'display' : 'block'
                    });    
                }      
            },
            onGetObject: function(obj) {

                if(obj.sheetName == 'mobs') {
                    if(mobIdSet.has(obj.id)) {
                        return;
                    } else {
                        mobIdSet.add(obj.id);
                    }
                } 
                if(obj.sheetName == 'objects') {
                    if(obj.name== 'Sun Carp') {
                        if(carpIdSet.has(obj.id)) {
                            return;
                        } else {
                            carpIdSet.add(obj.id);
                        }
                    }
                } 
                if(obj.layerName == 'objects') {
                    if(['Skyblossom', 'Emberleaf', 'Stinkcap'].includes(obj.name)) {
                        if(herbIdSet.has(obj.id)) {
                            return;
                        } else {
                            herbIdSet.add(obj.id);
                        }
                    }
                }
                if(obj.layerName == 'mobs') {
                    if(obj.ipHash) {

                        if(playerIdSet.has(obj.id)) {
                            return;
                        } else {
                            playerIdSet.add(obj.id);
                        }
                    }
                }

                if (mobIdSet.has(obj.id) && obj.destroyed == true) { 
                    mobIdSet.delete(obj.id);
                }
                if (carpIdSet.has(obj.id) && obj.destroyed == true) { 
                    carpIdSet.delete(obj.id);
                }
                if (herbIdSet.has(obj.id) && obj.destroyed == true) { 
                    herbIdSet.delete(obj.id);
                }
                if (playerIdSet.has(obj.id) && obj.destroyed == true) { 
                    playerIdSet.delete(obj.id);
                }
                
                $('.ui-container .uiObjectCounter .uiObjectCounterText').html(`Monster: ${mobIdSet.size} || Fishing Spots: ${carpIdSet.size} || Herbs: ${herbIdSet.size} || Players: ${playerIdSet.size-1}`)  
                
                if (typeof window.showObjectCounter !== 'undefined') {
                    if(window.showObjectCounter == false ) {
                        if(document.getElementById('uiObjectCounter').style.display == 'none') {
                            return;
                        } else {
                            document.getElementById('uiObjectCounter').style.display = 'none';
                        }
                    } else
                    if(window.showObjectCounter == true ) {
                        if(document.getElementById('uiObjectCounter').style.display == 'block') {
                            return;
                        } else {
                            document.getElementById('uiObjectCounter').style.display = 'block';
                        }
                    }
                }
            },
            onGetMap: function() {
                mobIdSet.clear();
                carpIdSet.clear();
                playerIdSet.clear();
                herbIdSet.clear();
            },
        });
    })
);
// ==UserScript==
// @name         Isleward - Respawn Timers
// @namespace    Isleward.Addon
// @version      1.5.0
// @description  Shows a respawn countdown at the position the client first registered the killed mob
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        let mobList = new Map();
        let defaultTime;
        function startCountdown(idX, xX, yX, timeX, nameX) {
            window.addons.events.emit("onGetObject", {id: idX, name: `${nameX} respawns in 60`, x: xX, y: yX});

            var repeat = function(){
                if(timeX > 0) {
                    timeX--;
                    window.addons.events.emit("onGetObject", {id: idX, name: `${nameX} respawns in ${timeX}`});
                }
                if (timeX <= 0) {
                    window.addons.events.emit("onGetObject", {id: idX, "destroyed": true});
                }
            };
            setInterval(repeat, 1000);       
        }      
        addons.register({
            init: function(events) {
                this.id = 'addon_respawn_timers';
                events.on('onGetObject', this.onGetObject.bind(this));
                events.on('onGetMap', this.onGetMap.bind(this));

            },
            onGetObject: function(obj) {        
                if (typeof window.showRespawnTimers !== 'undefined') {
                    if(window.showRespawnTimers == false ) {
                        return;
                    }
                }  

                if((obj.sheetName == 'mobs')        ||
                   (obj.name == 'Oryx Calf')        || (obj.name == 'Moontouched Oryx Calf')        ||
                   (obj.name == 'Oryx Matriarch')   || (obj.name == 'Moontouched Oryx Matriarch')   ||
                   (obj.name == 'Oryx Patriarch')   || (obj.name == 'Moontouched Oryx Patriarch')   ) {
                    if(mobList.has(obj.id)) {
                        return;
                    } else {
                        mobList.set(obj.id, [obj.x,obj.y, obj.name]);
                    }
                }

                if(obj.destructionEvent == 'visibility') {
                    if(mobList.has(obj.id)) {
                        mobList.delete(obj.id);    
                    }
                }
                if(obj.destructionEvent == 'death') {
                    if(mobList.has(obj.id)) {
                        if(window?.player?.zoneName === 'estuary') {
                            defaultTime = 139;
                        } else 
                        if(window?.player?.zoneName === 'blisterwind') {
                            defaultTime = 30;
                        } else {
                            defaultTime = 60;
                        }
                        if(window?.player?.zoneName === 'abyssalTrial') {
                            return;
                        }
                        if(window?.player?.zoneName === 'templeOfGaekatla') {
                            return;
                        }
                        startCountdown((obj.id + 1000000), mobList.get(obj.id)[0], mobList.get(obj.id)[1], defaultTime, mobList.get(obj.id)[2]);
                        mobList.delete(obj.id);
                    }
                } 
            },
            onGetMap: function() {   
                mobList.clear();
            }       
        });
    })
);

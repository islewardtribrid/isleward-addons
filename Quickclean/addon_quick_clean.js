// ==UserScript==
// @name         Isleward - Quick Cleanoff
// @namespace    Isleward.Addon
// @version      1.0.1
// @description  Allows players to quickly clean items by hovering over the item and pressing k
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
(function () {
    window.quickCleanData = {};
    window.quickCleanData.itemPos = -1;
    addons.register({
        init: function(events) {
            this.id = 'addon_quick_cleanoff';
            events.on('onShowItemTooltip', this.onShowItemTooltip.bind(this));
            events.on('onHideItemTooltip', this.onHideItemTooltip.bind(this));
            events.on('onKeyDown', this.onKeyDown.bind(this));
        },
        onShowItemTooltip: function(obj) {
            window.quickCleanData.itemPos = obj.pos;
        },
        onHideItemTooltip: function() {
            window.quickCleanData.itemPos = -1;
        },
        onKeyDown: function(key) {
            if (!key) {
                return;
            } else if (key == "k") {
                if(jQuery(".ui-container .uiInventory").css("display") == "block" && window.quickCleanData.itemPos != -1 && typeof jQuery(".uiMessages .active .typing")[0] === "undefined"){
                    jQuery(".ui-container .uiInventory .grid .item").eq(window.quickCleanData.itemPos).find(".icon").contextmenu();
                    for(var i=0;i< $(".uiContext .list .option").length;++i){
                        if(jQuery(".uiContext .list .option").eq(i).text() == "clean off"){
                            jQuery(".uiContext .list .option").eq(i).click();
                            break;
                        }
                    }
                }
            }
        },
    });
}));

// ==UserScript==
// @name         Isleward - Barrier Duration
// @namespace    Isleward.Addon
// @version      1.1.3
// @description  WIP
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        function startCountdownBarrier(durationX) {
            window.player.nameSprite.text = `${window.player.name} \nBarrier: ${durationX}`;
            var repeat = function(){
                if(durationX > 0) {
                    durationX--;
                    window.player.nameSprite.text = `${window.player.name} \nBarrier: ${durationX}`;
                }
                if (durationX <= 0) {
                    window.player.nameSprite.text = `${window.player.name}`;
                }
            };
            setInterval(repeat, 350);       
        };
        addons.register({
            init: function(events) {
                this.id = 'addon_barrier_duration';
                events.on('onGetObject', this.onGetObject.bind(this));
            },
            onGetObject: function(obj) {
                if (typeof window.showBarrierDuration !== 'undefined') {
                    if(window.showBarrierDuration == false ) {
                        return;
                    }
                }
                if(!obj) {
                    return;
                }
                if(!obj.components) {
                    return;
                }
                if(obj.components.length < 2) {
                    return
                }
                if(!obj.components[1].addEffects) {
                    return;
                }
                if(!obj.components[1].addEffects[0]) {
                    return;
                }
                if(obj.components[1].addEffects[0].type != 'mitigateDamage') {
                    return;
                }
                startCountdownBarrier(obj.components[1].addEffects[0].duration);
            }
        });
    })
);
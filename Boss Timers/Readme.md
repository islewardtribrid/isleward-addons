# Boss Timers

This addon adds a Bosstimer panel to the left side of the screen, it displays the respawn time.
![''](image2.png)

Additionally it also sends a message into the chat after every bosskill.
![''](image1.png)

If you do not want to use a specific timer, switch the value from that specific timer from true to false, follow the comment in the code at 'const timers'

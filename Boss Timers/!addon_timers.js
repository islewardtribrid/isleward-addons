// ==UserScript==
// @name         Isleward - Timer UI
// @namespace    Isleward.Addon
// @version      3.2.0
// @description  Adds the UI and logic for the Bosstimers
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        function toHHMMSS(secs) {
            var sec_num = parseInt(secs, 10);
            var hours = Math.floor(sec_num / 3600) % 24;
            var minutes = Math.floor(sec_num / 60) % 60;
            var seconds = sec_num % 60;
            return [hours,minutes,seconds]
                .map(v => v < 10 ? "0" + v : v)
                .filter((v,i) => v !== "00" || i > 0)
                .join(":")
        }
        const timers = [ 
            //0 tag 1 UI Name 2 Kills 3 RespawnTime 4 ID 5 Visibility 6 Key 7 Time
            ['mog',         'Mogresh',      0, 0, 0,    true,       null,   139],
            ['rad',         'Radulos',      0, 0, 0,    true,       null,   600],
            ['stink',       'Stinktooth',   0, 0, 0,    true,       null,   600],

            ['customTimer1Tag',   'CustomTimer1Name',     0, 0, 0,    false,       '16',   60],
            ['customTimer2Tag',   'CustomTimer2Name',     0, 0, 0,    false,       '17',   60],
            ['customTimer3Tag',   'CustomTimer3Name',     0, 0, 0,    false,       '18',   60],
            ['customTimer4Tag',   'CustomTimer4Name',     0, 0, 0,    false,       '19',   60]

        ];                                           //Change the value above 
        window.bosses = timers;                       //from true to false
                                                      //if you do not want a timer to be used
        addons.register({
            init: function(events) {
                this.id = 'addon_timers';
                events.on('onGetPlayer', this.onGetPlayer.bind(this));
                events.on('onGetObject', this.onGetObject.bind(this));
                events.on('onKeyDown', this.onKeyDown.bind(this));

            },
            onGetPlayer: function() {
                if (typeof(document.getElementById('addon-Left')) === 'undefined' || document.getElementById('addon-Left') === null) appendTimerPage();
                
                function appendTimerPage() {

                    this.left = $('<div id = "addon-Left" class="addon-Left"></div>').appendTo($('.ui-container'));  
                    this.left.css({
                        'position': 'absolute',
                        'left': '10px',
                        'top': '94px',
                        'z-index': '1'
                    });
                    this.addonTimers = $('<div id = "addon-Timers" class="addon-Timers"></div>').appendTo(this.left);
                    this.addonTimers.css({
                        'margin-top' : '10px',
                        'width' : '325px',
                        'visibility' : 'visible',
                        'box-shadow' : '0 -2px 0 #2d2136,0 2px 0 #2d2136,2px 0 0 #2d2136,-2px 0 0 #2d2136'
                    });
                    this.addonTimersHeading = $('<div id = "addon-Timers-Heading" class="addon-Timers-Heading">Bosses</div>').appendTo(this.addonTimers);
                    this.addonTimersHeading.css({
                        'color' : '#ffeb38',
                        'padding' : '8px',
                        'background-color' : 'rgba(58,59,74,.9)'
                    });
                    this.addonTimersList = $('<div id = "addon-Timers-List" class="addon-Timers-List"></div>').appendTo(this.addonTimers);
                }
                timers.forEach(timer=> {
                    if(document.getElementById(`addon-${timer[0]}`)){
                        return;
                    }
                    if(timer[5] == false) {
                        return;
                    }

                    this.timer = $(`<div id = "addon-${timer[0]}" class="addon-${timer[0]}"></div>`).appendTo($('.ui-container> .addon-Left .addon-Timers .addon-Timers-List'));
                    this.timer.css({
                        'cursor' : 'pointer',
                        'padding' : '8px',
                        'background-color' : 'rgba(58,59,74,.9)',
                        'color' : '#fcfcfc'
                    });
                    this.timerName = $(`<div id = "addon-${timer[0]}-Name" class="addon-${timer[0]}-Name">${timer[1]}</div>`).appendTo(this.timer);
                    this.timerName.css({
                        'margin-bottom' : '3px',
                        'color' : '#3fa7dd'
                    });
                    this.timerDesc = $(`<div id = "addon-${timer[0]}-Description" class="addon-${timer[0]}-Description">Fetching...</div>`).appendTo(this.timer);
                    this.timerDesc.css({
                        'color' : '#a3a3a3'
                    });
                    document.getElementById(`addon-${timer[0]}`).addEventListener('mouseover', function() {
                        document.getElementById(`addon-${timer[0]}`).style.backgroundColor = 'rgba(92,93,117,.9)';
                        document.getElementById(`addon-${timer[0]}-Description`).style.color = '#44cb95';

                    });
                    document.getElementById(`addon-${timer[0]}`).addEventListener('mouseout', function() {
                        document.getElementById(`addon-${timer[0]}`).style.backgroundColor = 'rgba(58,59,74,.9)';
                        document.getElementById(`addon-${timer[0]}-Description`).style.color = '#a3a3a3';
                    });
                });
                
            },
            onGetObject: function(obj) {
                if(obj.name === "m'ogresh") {
                    window.bosses[0][4] = obj.id;
                }
                if (typeof window.bosses[0][4] != "undefined" && obj.id == window.bosses[0][4] && obj.destructionEvent == 'death'){
                    window.bosses[0][3] = window.bosses[0][7];
                    window.bosses[0][2]++
                    $('<div class="list-message color-redA chat CombatLog">' + window.bosses[0][2]+' Mogresh Killed' + '</div>').appendTo($(".uiMessages .list"));
                    $(".uiMessages .list").scrollTop(9999999);
                }

                if(obj.name === "Radulos") {
                    window.bosses[1][4] = obj.id;
                }
                if (typeof window.bosses[1][4] != "undefined" && obj.id == window.bosses[1][4] && obj.destructionEvent == 'death'){
                    window.bosses[1][3] = window.bosses[1][7];
                    window.bosses[1][2]++
                    $('<div class="list-message color-redA chat CombatLog">' + window.bosses[1][2]+' Radulos Killed' + '</div>').appendTo($(".uiMessages .list"));
                    $(".uiMessages .list").scrollTop(9999999);
                }

                if(obj.name === "Stinktooth") {
                    window.bosses[2][4] = obj.id;
                }
                if (typeof window.bosses[2][4] != "undefined" && obj.id == window.bosses[2][4] && obj.destructionEvent == 'death'){
                    window.bosses[2][3] = window.bosses[2][7];
                    window.bosses[2][2]++
                    $('<div class="list-message color-redA chat CombatLog">' + window.bosses[2][2]+' Stinktooth Killed' + '</div>').appendTo($(".uiMessages .list"));
                    $(".uiMessages .list").scrollTop(9999999);
                }

                if (typeof window.showTimers !== 'undefined') {
                    if(window.showTimers == false ) {
                        if(document.getElementById('addon-Timers').style.display == 'none') {
                            return;
                        } else {
                            document.getElementById('addon-Timers').style.display = 'none';
                        }
                    } else
                    if(window.showTimers == true ) {
                        if(document.getElementById('addon-Timers').style.display == 'block') {
                            return;
                        } else {
                            document.getElementById('addon-Timers').style.display = 'block';
                        }
                    }
                }
            },
            onKeyDown: function(key) {
                if(key == window.bosses[3][6]) { // F1
                    window.bosses[3][3] = window.bosses[3][7];
                } else
                if(key == window.bosses[4][6]) { // F2
                    window.bosses[4][3] = window.bosses[4][7];
                } else
                if(key == window.bosses[5][6]) { // F3
                    window.bosses[5][3] = window.bosses[5][7];
                } else
                if(key == window.bosses[6][6]) { // F3
                    window.bosses[6][3] = window.bosses[6][7];
                }
                
            }
        });

        var repeat = function(){
            if(window.bosses[0][3] > 0){
                window.bosses[0][3]--;
            }
            if(window.bosses[1][3] > 0){
                window.bosses[1][3]--;
            }
            if(window.bosses[2][3] > 0){
                window.bosses[2][3]--;
            }
            if(window.bosses[3][3] > 0){
                window.bosses[3][3]--;
            }
            if(window.bosses[4][3] > 0){
                window.bosses[4][3]--;
            }
            if(window.bosses[5][3] > 0){
                window.bosses[5][3]--;
            }
            if(window.bosses[6][3] > 0){
                window.bosses[6][3]--;
            }

            $(`.ui-container .addon-Left .addon-Timers .addon-Timers-List .addon-${window.bosses[0][0]} .addon-${window.bosses[0][0]}-Description`).html(`Respawns in: ${toHHMMSS(window.bosses[0][3])}`);
            $(`.ui-container .addon-Left .addon-Timers .addon-Timers-List .addon-${window.bosses[1][0]} .addon-${window.bosses[1][0]}-Description`).html(`Respawns in: ${toHHMMSS(window.bosses[1][3])}`);
            $(`.ui-container .addon-Left .addon-Timers .addon-Timers-List .addon-${window.bosses[2][0]} .addon-${window.bosses[2][0]}-Description`).html(`Respawns in: ${toHHMMSS(window.bosses[2][3])}`);
            $(`.ui-container .addon-Left .addon-Timers .addon-Timers-List .addon-${window.bosses[3][0]} .addon-${window.bosses[3][0]}-Description`).html(`Respawns in: ${toHHMMSS(window.bosses[3][3])}`);
            $(`.ui-container .addon-Left .addon-Timers .addon-Timers-List .addon-${window.bosses[4][0]} .addon-${window.bosses[4][0]}-Description`).html(`Respawns in: ${toHHMMSS(window.bosses[4][3])}`);
            $(`.ui-container .addon-Left .addon-Timers .addon-Timers-List .addon-${window.bosses[5][0]} .addon-${window.bosses[5][0]}-Description`).html(`Respawns in: ${toHHMMSS(window.bosses[5][3])}`);
            $(`.ui-container .addon-Left .addon-Timers .addon-Timers-List .addon-${window.bosses[6][0]} .addon-${window.bosses[6][0]}-Description`).html(`Respawns in: ${toHHMMSS(window.bosses[6][3])}`);
        };
        setInterval(repeat, 1000);
    })
);
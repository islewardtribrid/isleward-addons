// ==UserScript==
// @name         Isleward - Additional Settings
// @namespace    Isleward.Addon
// @version      2.3.1
// @description  Adds Additional Settings to the menu, working with all my other addons
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                this.id = 'addon_settings';
                events.on('onOpenOptions', this.onOpenOptions.bind(this));
                events.on('onCloseOptions', this.onCloseOptions.bind(this));
                events.on('onGetPlayer', this.onGetPlayer.bind(this));
            },
            onOpenOptions: function() {
                document.getElementById('addon-Options-Left').style.display = 'flex';
                document.getElementById('addon-Options-Right').style.display = 'flex';
            },
            onCloseOptions: function() {
                document.getElementById('addon-Options-Left').style.display = 'none';
                document.getElementById('addon-Options-Right').style.display = 'none';
            },
            onGetPlayer: function() {
                var buttons = [
                    ['showCombatLog',           'Show Combat Log',              JSON.parse(localStorage['showCombatLog']            ?? 'false'),  'left'],
                    ['showProphecy',            'Show Player Prophecy',         JSON.parse(localStorage['showProphecy']             ?? 'false'),  'left'],
                    ['showAggressive',          'Show Aggressive Mobs',         JSON.parse(localStorage['showAggressive']           ?? 'false'),  'left'],
                    ['showQuestmob',            'Show Questmobs',               JSON.parse(localStorage['showQuestmob']             ?? 'false'),  'left'],
                    ['showRezoneAnnouncement',  'Show Rezone Announcement',     JSON.parse(localStorage['showRezoneAnnouncement']   ?? 'false'),  'left'],
                    ['showLoginLogoff',         'Show Login/Logoff Messages',   JSON.parse(localStorage['showLoginLogoff']          ?? 'false'),  'left'],
                    ['showTimestamp',           'Show Chat Timestamp',          JSON.parse(localStorage['showTimestamp']            ?? 'false'),  'left'],
                    ['showTimers',              'Show Timers',                  JSON.parse(localStorage['showTimers']               ?? 'false'),  'left'],
                    ['showAnnouncements',       'Show Announcements',           JSON.parse(localStorage['showAnnouncements']        ?? 'false'),  'left'],
                    ['showReputation',          'Show Faction Reputation',      JSON.parse(localStorage['showReputation']           ?? 'false'),  'left'],
                    ['showCoordinates',         'Show Coordinates',             JSON.parse(localStorage['showCoordinates']          ?? 'false'),  'left'],
                    ['showObjectCounter',       'Show Object Counter',          JSON.parse(localStorage['showObjectCounter']        ?? 'false'),  'left'],
                    ['rejectFriendRequest',     'Reject Friend Requests',       JSON.parse(localStorage['rejectFriendRequest']      ?? 'false'),  'left'],
                    ['rejectPartyInvite',       'Reject Party Invites',         JSON.parse(localStorage['rejectPartyInvite']        ?? 'false'),  'left'],
                    ['showRespawnTimers',       'Show Respawn Timers',          JSON.parse(localStorage['showRespawnTimers']        ?? 'false'),  'left'],
                    ['showDecoyDuration',       'Show Decoy Duration',          JSON.parse(localStorage['showDecoyDuration']        ?? 'false'),  'left'],
                    ['showBarrierDuration',     'Show Barrier Duration',        JSON.parse(localStorage['showBarrierDuration']      ?? 'false'),  'left'],
                    ['showReputationOverall',   'Show Reputation Overall',      JSON.parse(localStorage['showReputationOverall']    ?? 'false'),  'left']
                ];
                
                if (typeof(document.getElementById('addon-Options-Left')) === 'undefined' || document.getElementById('addon-Options-Left') === null) {
                    appendAddonSettingsPages('Left');
                };
                if (typeof(document.getElementById('addon-Options-Right')) === 'undefined' || document.getElementById('addon-Options-Right') === null) {
                    appendAddonSettingsPages('Right');
                };
                
                function appendAddonSettingsPages(location) {
                            
                    this.addonOptions = $(`<div id = "addon-Options-${location}" class="addon-Options-${location}"></div>`).appendTo($('.ui-container'));
                    this.addonOptions.css({
                        'position': 'absolute',
                        'top': $('.ui-container .uiOptions')[0].style.top,
                        'display' : 'none',
                        'width': '20%',
                        'background-color': '#373041',
                        'border': '5px solid #3c3f4c',
                        'flex-direction': 'column'
                    });
                    document.getElementById(`addon-Options-${location}`).style[location.toLowerCase()] = '5%'

                    this.addonOptionsHeading = $(`<div id = "addon-Options-${location}-Heading" class="addon-Options-${location}-Heading"></div>`).appendTo(this.addonOptions);
                    this.addonOptionsHeading.css({
                        'color' : '#48edff',
                        'width' : '100%',
                        'height' : '36px',
                        'background-color' : '#3c3f4c',
                        'position' : 'relative',
                        'text-align' : 'center'
                    });
        
                    this.addonOptionsHeadingText = $(`<div id = "addon-Options-${location}-Heading-Text" class="addon-Options-${location}-Heading-Text">Addon Options</div>`).appendTo(this.addonOptionsHeading);
                    this.addonOptionsHeadingText.css({
                        'padding-top' : '8px',
                        'margin' : 'auto' 
                    });
        
                    this.addonOptionsBottom = $(`<div id = "addon-Options-${location}-Bottom" class="addon-Options-${location}-Bottom"></div>`).appendTo(this.addonOptions);
                    this.addonOptionsBottom.css({
                        'padding': '10px',
                        'height': '100%',
                        'overflow-y': 'auto'
                    });
                    this.addonOptionsBottomList = $(`<div id = "addon-Options-${location}-Bottom-List" class="addon-Options-${location}-Bottom-List"></div>`).appendTo(this.addonOptionsBottom);
                    this.addonOptionsBottomList.css({
                        'display': 'flex',
                        'flex-direction': 'column'
                    });
        
                };

                buttons.forEach(button=> {
                    if(document.getElementById(`addon-${button[0]}`)){
                        return;
                    }

                    window[button[0]] = button[2];
        
                    if(button[3] === 'left') {
                        this.option = $(`<div id = "addon-${button[0]}" class="addon-${button[0]}"></div>`).appendTo($('.ui-container> .addon-Options-Left .addon-Options-Left-Bottom .addon-Options-Left-Bottom-List'));
                        this.option.css({
                            'height' : '30px',
                            'display' : 'flex',
                            'justify-content' : 'space-between',
                            'align-items' : 'center'
                        });
                    } else 
                    if(button[3] === 'right') {
                        this.option = $(`<div id = "addon-${button[0]}" class="addon-${button[0]}"></div>`).appendTo($('.ui-container> .addon-Options-Right .addon-Options-Right-Bottom .addon-Options-Right-Bottom-List'));
                        this.option.css({
                            'height' : '30px',
                            'display' : 'flex',
                            'justify-content' : 'space-between',
                            'align-items' : 'center'
                        });
                    }
                    this.optionName = $(`<div id = "addon-${button[0]}-Name" class="addon-${button[0]}-Name">${button[1]}</div>`).appendTo(this.option);
                    this.optionName.css({
                        'color': '#3fa7dd',
                        'flex' : '1',
                        'height' : '100%',
                        'display' : 'flex',
                        'align-items' : 'center',
                        'padding' : '0 10px',
                        'margin-right' : '10px',
                        'cursor' : 'pointer'
                    });
                    this.optionValue = $(`<div id = "addon-${button[0]}-Value" class="addon-${button[0]}-Value">${button[2]}</div>`).appendTo(this.option);
                    this.optionValue.css({
                        'color': '#fcfcfc',
                        'display' : 'flex',
                        'align-items' : 'center',
                        'justify-content' : 'flex-end',
                        'padding-right' : '10px'
                    });
                                              
                    document.getElementById(`addon-${button[0]}-Name`).addEventListener('mouseover', function() {
                        document.getElementById(`addon-${button[0]}-Name`).style.backgroundColor = '#505360';
                        document.getElementById(`addon-${button[0]}-Name`).style.color = '#48edff';
                    });
                    document.getElementById(`addon-${button[0]}-Name`).addEventListener('mouseout', function() {
                        document.getElementById(`addon-${button[0]}-Name`).style.backgroundColor = '#373041';
                        document.getElementById(`addon-${button[0]}-Name`).style.color = '#3fa7dd';
                    });
                    document.getElementById(`addon-${button[0]}-Name`).addEventListener('click', function() {
                        switchButtonState(button);    
                    });
                });

                function switchButtonState(obj) {
                    if(obj[2] === true) {
                        obj[2] = false;
                        localStorage[obj[0]] = false;
                    } else
                    if(obj[2] === false) {
                        obj[2] = true;
                        localStorage[obj[0]] = true;
                    }
                    if(obj[3] === 'left') {
                        $(`.ui-container> .addon-Options-Left .addon-Options-Left-Bottom .addon-Options-Left-Bottom-List .addon-${obj[0]}-Value`).html(obj[2]+'');
                    } else
                    if(obj[3] === 'right') {
                        $(`.ui-container> .addon-Options-Right .addon-Options-Right-Bottom .addon-Options-Right-Bottom-List .addon-${obj[0]}-Value`).html(obj[2]+'');
                    }
                         
                    window[obj[0]] = obj[2];     
                }
            },
        });
    })
);
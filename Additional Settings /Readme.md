# Additional Settings

This Addon adds settings to the menu, those settings affect the following addons:

- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Boss%20Timers
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Show%20Aggressive%20entities
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Show%20Announcements
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Show%20Chat%20Timestamp
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Show%20Combat%20Log
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Show%20Coordinates
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Show%20Faction%20Reputation
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Show%20Player%20Prophecy
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Show%20Questmobs
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Show%20Rezone%20Announcement
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Hide%20Login-Logoff%20Messages
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Object%20Counter
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Reject%20Friend%20Requests
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Reject%20Party%20Invites
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Decoy%20Duration
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Respawn%20Timers
- https://gitlab.com/islewardtribrid/isleward-addons/-/tree/main/Barrier%20Duration

When having the Settings addon, all of the addons are toggleable, when you have those addons and do NOT have the settings, the addons are automatically active.

![''](image.png)
